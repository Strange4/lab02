// My name is Mauricio Murillo
public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] bikes = new Bicycle[4];
        bikes[0] = new Bicycle("Giant", 12, 50.5);
        bikes[1] = new Bicycle("Specialized", 10, 55.5);
        bikes[2] = new Bicycle("Fixie", 1, 40);
        bikes[3] = new Bicycle("Bikes", 9, 50.5);
        for (Bicycle bike :
             bikes) {
            System.out.println(bike);
        }
    }
}
